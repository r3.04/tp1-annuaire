# TP Annuaire (Modularité)

Dans ce TP, nous allons repartir d’un TP réalisé au S2 en Développement Orienté Objet pour revoir les notions de polymorphisme et appliquer celles de modularité.
Ça sera aussi l'occasion de revoir la documentation, les tests et leur couverture ainsi que les fonctionnalités proposées IntelliJ à ces niveaux.

## Fork

Sur ce dépôt, en plus du sujet (ce document), vous trouverez le code source de classes Java existantes desquelles nous allons repartir.

Créez une divergence (*fork* en anglais) de ce projet de manière à en avoir une copie sur un dépôt distant qui vous appartienne et sur lequel vous pourrez faire des modifications sans altérer l'original. 

<a>![](img/fork.png)</a>

## Cloner depuis l'environnement de développement intégré

Pour réaliser ce TP (comme les précédents et les suivants), nous vous conseillons fortement d'utiliser IntelliJ comme Environnement de Développement Intégré (*IDE* en anglais).

Il est possible de créer directement un nouveau projet à partir d'IntelliJ : 
1. dans le menu projet d'IntelliJ, choisissez *Get from Version Control...*
<a>![](img/intelliJ-import-from-remote-url.png)</a> 

2. une fenêtre s'ouvre dans laquelle vous pouvez coller l'URL du dépôt distant à cloner :
<a>![](img/intelliJ-import-from-remote-url-2.png)</a> 

## Replongeons dans ce code

Note : Pour la pertinence de ce sujet, des adaptations ont été apportées à ce code qui ne correspond pas tout à fait à celui que vous aviez à faire au S2 mais en reprend les grandes lignes.

### Exécution et fonctionnalités

Un point de départ qui permet de se mettre dans le contexte est souvent de passer par une première exécution, en particulier si un *main* est disponible à cette fin. C'est aussi un moyen de s'assurer d'une configuration correcte de notre IDE et du fonctionnement au moins partiel du logiciel avant toute modification.

1. Ouvrez la classe *src/devoo/tp07/ConsolePhoneBookManager*
2. Il est possible qu'IntelliJ vous indique *Project JDK is not defined*, auquel cas vous pouvez en définir un : *Setup SDK* -> *17*. Ce qui signifie que le *Java Development Kit* qui sera utilisé pour compiler et exécuter correspondra à la version 17.
3. Lancez le *main* de cette classe et essayez un peu le programme.

### UML et Documentation

On peut aussi mieux comprendre un logiciel existant par un diagramme de classes lorsqu'il est fourni :

<a>![](img/uml.png)</a> 

Une autre manière d'appréhender le projet et par le code en lui-même, en se référant à sa documentation (d'où l'intérêt de l'écrire...!). Ici, le code est plutôt bien documenté. 

[Générez la documentation](https://www.jetbrains.com/help/idea/javadocs.html#generate-javadoc) en tapant JavaDoc *javadoc* dans le menu interactif d'IntelliJ (qu'on peut obtenir pour rappel en appuyant deux fois sur *Shift*).

Un certain nombre d'options sont proposées, vous pouvez garder les choix par défaut en choisissant l' *output directory* dans lequel générer la documentation, par exemple un répertoire *doc* dans votre projet.
<a>![](img/generate_javadoc.png)</a> 

Cela peut aussi être fait simplement en ligne de commande depuis le répertoire *annuaire* :

```
javadoc -d doc src/main/java/devoo/tp07/*.java
```

Explorez ensuite la documentation générée, en consultant notamment les méthodes disponibles. Dans la suite du TP, dès que vous modifierez ou ajouterez du code, pensez à le documenter.

## Quelques améliorations

### Paquetages et modularité

Au S2, en Dév-oo, nous avions pris l'habitude de travailler au sein du même projet et d'associer à chaque sujet de tp un paquetage différent. Durant ce semestre, en qualité de développement, la règle générale sera que chaque sujet de TP constitue un projet à part entière. Ainsi, les paquetages serviront à l'architecture et à la modularité de votre code.

Dans cet esprit, nous allons ici commencer par réorganiser le code dans une optique d'API (*Application Programming Interface*). On sort donc de la dimension scolaire pour un nommage plus professionnel à visée universelle. On va donc inclure les classes présentes dans un paquetage **fr.univlille.phonebook**. Pour ce faire, on peut utiliser les outils de refactorisation de code, plus ou moins ergonomiques selon l'IDE, pour un renommage global sans aller changer chaque fichier "à la main". Sur IntelliJ, un simple rename (Maj + F6) sur le paquetage suffit à renommer l'ensemble des paquetages, y compris ceux côtés test.

<a>![](img/intelliJ-rename.png)</a> 


À quoi la classe *ConsolePhoneBookManager* a-t-elle accès dans *PhoneBook* ?
Trouvez une solution de manière à mieux respecter le principe de modularité.

Une fois ces opérations effectuées, mettez à jour votre dépôt git distant en veillant à ne pas le polluer par d'éventuels fichiers non pertinents sur le dépôt. Vous pouvez reprendre vos cours précédents si vous avez oublié ou demander à votre enseignant.

#### Static

Dans la classe *ConsolePhoneBookManager*, différentes méthodes recourent à un même objet de manière relativement implicite. Cela ne respecte pas les principes de modularité. Réfléchissez à une solution de manière à éviter ce problème et proposez-la à votre enseignant avant de la mettre en oeuvre. Cette solution devrait notamment permettre de passer facilement à la gestion de 2 annuaires.

### Résolution de bug

Un bug a été signalé concernant l'application, il se produirait lorsqu'on veut consulter les coordonnées d'un correspondant absent de l'annuaire. Identifiez le problème et apportez lui une solution en terme de code et surtout de documentation. Une fois le correctif apporté, mettez à jour le dépôt distant.

### Qualité

Un des éléments majeurs pour augmenter la valeur du code et justement d'éviter la présence de bugs. Pour ce faire, recourez à des tests pertinents qui garantissent le bon comportement des méthodes.

Ici, des tests ont été réalisés pour une partie de la classe *PhoneNumber*. Réalisez-en d'autres complémentaires ainsi que pour les classes *ULillePhoneNumber* et *PhoneBook*. L'objectif est de vous assurer du bon fonctionnement de ses méthodes dans divers scénarios et d'atteindre une couverture maximale. 

Sur intelliJ, la couverture de code de vos tests peut être visualisée en faisant un clic droit sur la classe à tester et en choissisant *Run ...Test with coverage* 
<a>![](img/test_coverage_1.png)</a> 

On obtient alors une vue avec pour chaque classe le pourcentage de méthodes/lignes/branches testées pour chaque classe.
<a>![](img/test_coverage_2.png)</a> 

L'intérêt des tests est aussi de pouvoir détecter une éventuelle future erreur de manière automatisée (pour peu qu'on les exécute régulièrement), sans avoir à essayer "à la main" une série de scénarios.

Une fois vos tests réalisés, pensez de nouveau à mettre à jour votre dépôt distant. 
Dans la suite de ce sujet, il n'y aura plus d'indication concernant les commits à réaliser. Ça sera donc à vous d'aviser pour les effectuer de manière régulière et pertinente.

## Ajout de nouvelles fonctionnalités

### Intégration de la classe ULillePhoneNumber

Pour l'instant, à quel endroit la classe *ULillePhoneNumber* est-elle utilisée ? 
Pour répondre à cette question, les IDE proposent généralement une fonctionnalité (Clic droit + *Find Usages* ou Alt + F7 sur IntelliJ).

Modifiez le logiciel pour qu'il soit désormais aussi possible d'ajouter un numéro interne à l'université à partir uniquement du département et du numéro de poste. Il s'agit d'un numéro plus court, uniquement constitué des 5 derniers chiffres, on considérera ici que les premiers sont les mêmes quelque soit le poste (ça n'est pas tout à fait vrai en réalité). 

À l'aide d'une méthode de fabrication statique, faîtes en sorte que l'on appelle le constructeur adéquat selon qu'on saisissse un numéro interne (par ex. *32106*), national (*03.20.21.22.23*) ou international (*+34.6.74.11.22*).

*Méthode de création statique, à décliner sur les PhoneNumber :*

<a>![](img/static-factory.svg)</a>


N'oubliez pas de documenter.

### Affichage du numéro de poste interne

On vous a commandé l'ajout d'une nouvelle fonctionnalité : la récupération du numéro interne à l'Université.
Le scénario est le suivant : 
On souhaite passer un appel depuis l'Université vers un poste de l'Université. On récupére donc le numéro de poste interne plutôt que le numéro en entier.
Implémentez cette fonctionnalité.
Attention, si on appelle depuis l'Université vers l'extérieur, on doit continuer à récupérer le numéro en entier.

### Mise à jour de numéro de téléphone

Il arrive parfois qu'un numéro poste corresponde à plusieurs utilisateurs. Dans ce cas, lors d'une mise à jour du numéro, il faut pouvoir la répercuter sur l'ensemble des contacts concernés, en remplaçant leur ancien numéro par le nouveau. Réfléchissez à une solution pour ajouter cette fonctionnalité à l'application. Présentez votre idée à votre enseignant diagramme de classes à l'appui.

Après validation de votre enseignant.e, implémentez la fonctionnalité ainsi que les tests correspondants.


## Généralisation

L'application est aussi susceptible de servir pour d'autres universités, voire des entreprises, disposant elles aussi de numéros de postes internes. Les numéros internes de l'Université sont sur 5 chiffres mais il pourrait en être autrement pour d'autres structures. Qu'y a-t-il à changer dans notre prototype pour qu'il corresponde à ce besoin ?


Discutez-en avec votre enseignant puis effectuez ces modifications.

### Annuaire à doubles entrées

Il arrive qu'une même personne soit affectée à 2 bureaux, c'est par exemple souvent le cas des enseignants-chercheurs.
On souhaite donc pouvoir disposer d'annuaires permettant d'associer à une même personne deux numéros de téléphones. L'application devra rester compatible avec des annuaires mono-numéro.

Comment faire ? 

Conseil : s'appuyer sur les pattern Strategy et Decorator (ou éventuellement Composite)

Dessiner un UML et proposer votre solution à votre enseignant.


## Bonus

Implémentez des tests pour les parties du code qui ne sont pas couvertes actuellement (hors entrées/sorties). À noter qu'il existe des outils pour mesurer la couverture de code des tests.

Choisissez une fonctionnalité à ajouter à votre application, réfléchissez à une solution en terme de conception puis essayez de l'implémenter.
