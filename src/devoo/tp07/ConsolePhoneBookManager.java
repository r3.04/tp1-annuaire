package devoo.tp07;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ConsolePhoneBookManager {

    private static final Scanner scan = new Scanner(System.in);
    private static PhoneBook phoneBook;

    public static void main(String[] args) {
        phoneBook = new PhoneBook();
        indicatifConfig();
        menu();
    }

    private static void indicatifConfig() {
        int choiceNum = askChoice("Souhaitez-vous passer des appels depuis", "la France", "l'international");

        if (choiceNum == 0){
            PhoneNumber.setCountryCodeReference(PhoneNumber.FRANCE_CODE);
        }else if (choiceNum == 1){
            PhoneNumber.setCountryCodeReference(askCountryCode());
        }
    }

    private static int askCountryCode() {
        System.out.println("Entrez l'indicatif du pays : ");
        String answer = scan.nextLine();
            try{
                int userCountryCode = Integer.parseInt(answer);
                if (userCountryCode<=0)
                    throw new NumberFormatException();
                else
                    return userCountryCode;
            }catch(NumberFormatException nfe){
                System.out.println("Valeur incorrecte : nombre positif attendu");
                return askCountryCode();
            }
    }

    private static int askChoice(String question, String... choices) {
        System.out.println(question);
        int num=1;
        for(String choice : choices)
            System.out.println(num++ + " - " + choice);

        try{
            int choice = scan.nextInt()-1;
            if (choice>=0 && choice < choices.length){
                scan.nextLine();
                return choice;
            }
            else
                throw new InputMismatchException();
        }catch(InputMismatchException ime){
            scan.nextLine();
            return askChoice(question, choices);            
        }
    }

    private static void menu() {
        System.out.println("Que souhaitez-vous faire ?");
        System.out.println("\t* : afficher tout le répertoire");
        System.out.println("\t+ : ajouter une entrée");
        System.out.println("\t- : supprimer une entrée");
        System.out.println("\t? : consulter les coordonnées téléphoniques d'un correspondant particulier");        
        System.out.println("\tq : quitter le programme");
        chooseAction();
    }

    private static void chooseAction() {        
        String input = scan.nextLine();
        switch(input){
            case "*":
                for (String name : phoneBook)
                    System.out.println(name + " - " + phoneBook.getNumber(name).toDial());
                break;
            case "+": addEntry();  break;
            case "-": removeEntry();  break;
            case "?": getNumber();  break;
            case "q": scan.close(); System.exit(0); break;
            default : break; 
        } 
        menu();      
    }

    private static void getNumber() {
        String name = askName();
        System.out.println(phoneBook.getNumber(name).toDial());
    }

    private static String askName(){
        System.out.println("Entrez un nom ");
        return scan.nextLine();
    }

    private static void removeEntry() {
        String name = askName();
        phoneBook.remove(name);
    }

    private static void addEntry() {
        int area, sector, one, two, three;
        String name = askName();

        System.out.println("Entrez un indicatif");
        int country = askCountryCode();

        System.out.println("Entrez un numéro sous la forme xx.xx.xx.xx.xx");
        scan.useDelimiter("\\.|\n");
        area = scan.nextInt();
        sector = scan.nextInt();
        one = scan.nextInt();
        two = scan.nextInt();
        three = scan.nextInt();

        phoneBook.put(name,new PhoneNumber(country, area, sector, one, two, three));
        scan.nextLine();
        scan.reset();
    }
    
}
