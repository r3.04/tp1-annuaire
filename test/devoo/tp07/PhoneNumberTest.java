package devoo.tp07;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PhoneNumberTest {

    private PhoneNumber nationalNumber;

    @BeforeEach
    public void setUp(){
        nationalNumber = new PhoneNumber(33, 3, 59, 3, 21, 6);
    }

    @Test
    public void testStandardFormat(){
        Assertions.assertEquals("03.59.03.21.06", nationalNumber.standardFormat());
    }

    @Test
    public void testInternationalFormat(){
        Assertions.assertEquals("+33.3.59.03.21.06", nationalNumber.internationalFormat());
    }

}
